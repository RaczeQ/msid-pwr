# --------------------------------------------------------------------------
# -----------------------  Rozpoznawanie Obrazow  --------------------------
# --------------------------------------------------------------------------
#  Zadanie 2: k-NN i Naive Bayes
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

from __future__ import division
import numpy as np


def hamming_distance(X, X_train):
    """
    :param X: zbior porownwanych obiektow N1xD
    :param X_train: zbior obiektow do ktorych porownujemy N2xD
    Funkcja wyznacza odleglosci Hamminga obiektow ze zbioru X od
    obiektow X_train. ODleglosci obiektow z jednego i drugiego
    zbioru zwrocone zostana w postaci macierzy
    :return: macierz odleglosci pomiedzy obiektami z X i X_train N1xN2
    """
    np_X = X.toarray()
    np_X_train = X_train.toarray()
    size = np_X_train.shape[0]
    return [np.count_nonzero(np.tile(row, (size, 1)) != np_X_train, 1) for row in np_X]

def sort_train_labels_knn(Dist, y):
    """
    Funkcja sortujaca etykiety klas danych treningowych y
    wzgledem prawdopodobienstw zawartych w macierzy Dist.
    Funkcja zwraca macierz o wymiarach N1xN2. W kazdym
    wierszu maja byc posortowane etykiety klas z y wzgledem
    wartosci podobienstw odpowiadajacego wiersza macierzy
    Dist
    :param Dist: macierz odleglosci pomiedzy obiektami z X
    i X_train N1xN2
    :param y: wektor etykiet o dlugosci N2
    :return: macierz etykiet klas posortowana wzgledem
    wartosci podobienstw odpowiadajacego wiersza macierzy
    Dist. Uzyc algorytmu mergesort.
    """
    return np.apply_along_axis(lambda row: y[np.argsort(row, kind='mergesort')], 1, Dist)

def p_y_x_knn(y, k):
    """
    Funkcja wyznacza rozklad prawdopodobienstwa p(y|x) dla
    kazdej z klas dla obiektow ze zbioru testowego wykorzystujac
    klasfikator KNN wyuczony na danych trenningowych
    :param y: macierz posortowanych etykiet dla danych treningowych N1xN2
    :param k: liczba najblizszych sasiadow dla KNN
    :return: macierz prawdopodobienstw dla obiektow z X
    """
    return [[np.count_nonzero(row[:k] == i) / k for i in [1, 2, 3, 4]] for row in y]

def classification_error(p_y_x, y_true):
    """
    Wyznacz blad klasyfikacji.
    :param p_y_x: macierz przewidywanych prawdopodobienstw
    :param y_true: zbior rzeczywistych etykiet klas 1xN.
    Kazdy wiersz macierzy reprezentuje rozklad p(y|x)
    :return: blad klasyfikacji
    """
    return np.sum(y_true != (4 - np.argmax(np.fliplr(p_y_x), 1))) / len(y_true)

def model_selection_knn(Xval, Xtrain, yval, ytrain, k_values):
    """
    :param Xval: zbiór danych walidacyjnych N1xD
    :param Xtrain: zbiór danych treningowych N2xD
    :param yval: etykiety klas dla danych walidacyjnych 1xN1
    :param ytrain: etykiety klas dla danych treningowych 1xN2
    :param k_values: wartosci parametru k, ktore maja zostac sprawdzone
    :return: funkcja wykonuje selekcje modelu knn i zwraca krotke (best_error,best_k,errors), gdzie best_error to najnizszy
    osiagniety blad, best_k - k dla ktorego blad byl najnizszy, errors - lista wartosci bledow dla kolejnych k z k_values
    """
    labels = sort_train_labels_knn(hamming_distance(Xval, Xtrain), ytrain)
    errors = []
    best_error = 2
    best_k = 0
    for k in k_values:
        error = classification_error(p_y_x_knn(labels, k), yval)
        errors.append(error)
        if error < best_error:
            best_k = k
            best_error = error
    return best_error, best_k, errors


def estimate_a_priori_nb(ytrain):
    """
    :param ytrain: etykiety dla dla danych treningowych 1xN
    :return: funkcja wyznacza rozklad a priori p(y) i zwraca p_y - wektor prawdopodobienstw a priori 1xM
    """
    size = ytrain.shape[0]
    return [np.count_nonzero(ytrain == i) / size for i in [1, 2, 3, 4]]


def estimate_p_x_y_nb(Xtrain, ytrain, a, b):
    """
    :param Xtrain: dane treningowe NxD
    :param ytrain: etykiety klas dla danych treningowych 1xN
    :param a: parametr a rozkladu Beta
    :param b: parametr b rozkladu Beta
    :return: funkcja wyznacza rozklad prawdopodobienstwa p(x|y) zakladajac, ze x przyjmuje wartosci binarne i ze elementy
    x sa niezalezne od siebie. Funkcja zwraca macierz p_x_y o wymiarach MxD.
    """
    np_Xtrain = Xtrain.toarray()
    size_d = np_Xtrain.shape[1]
    result = np.zeros(shape=(4, size_d))
    for k in [1, 2, 3, 4]:
        denominator = np.count_nonzero(ytrain == k)
        for d in range(size_d):
            numerator = np.count_nonzero((ytrain == k) & np_Xtrain[:,d]) # [:,d] => cała kolumna d
            result[k - 1][d] = np.sum((numerator + a - 1) / (denominator + a + b - 2))
    return result


def p_y_x_nb(p_y, p_x_1_y, X):
    """
    :param p_y: wektor prawdopodobienstw a priori o wymiarach 1xM
    :param p_x_1_y: rozklad prawdopodobienstw p(x=1|y) - macierz MxD
    :param X: dane dla ktorych beda wyznaczone prawdopodobienstwa, macierz NxD
    :return: funkcja wyznacza rozklad prawdopodobienstwa p(y|x) dla kazdej z klas z wykorzystaniem klasyfikatora Naiwnego
    Bayesa. Funkcja zwraca macierz p_y_x o wymiarach NxM.
    """
    np_X =  X.toarray()
    size = np_X.shape[0]
    result = np.zeros(shape=(size, 4))
    for n in range(size):
        p_x_y = []
        denominator = 0
        # Oblicz prawdopodobieństwo dla aktualnego rzędu dla każdego k, oraz oblicz mianownik (taki sam dla każdego k)
        for k in [1, 2, 3, 4]:
            prod = np.prod((p_x_1_y[k - 1]**np_X[n])*((1-p_x_1_y[k - 1])**(1-np_X[n])))
            p_x_y.append(prod)
            denominator += prod * p_y[k - 1]
        # Dla każdego k (wymiar m) podstaw do wzoru
        for m in [1, 2, 3, 4]:
            numerator = p_x_y[m - 1] * p_y[m - 1]
            result[n][m - 1] = numerator / denominator
    return result

def model_selection_nb(Xtrain, Xval, ytrain, yval, a_values, b_values):
    """
    :param Xtrain: zbior danych treningowych N2xD
    :param Xval: zbior danych walidacyjnych N1xD
    :param ytrain: etykiety klas dla danych treningowych 1xN2
    :param yval: etykiety klas dla danych walidacyjnych 1xN1
    :param a_values: lista parametrow a do sprawdzenia
    :param b_values: lista parametrow b do sprawdzenia
    :return: funkcja wykonuje selekcje modelu Naive Bayes - wybiera najlepsze wartosci parametrow a i b. Funkcja zwraca
    krotke (error_best, best_a, best_b, errors) gdzie best_error to najnizszy
    osiagniety blad, best_a - a dla ktorego blad byl najnizszy, best_b - b dla ktorego blad byl najnizszy,
    errors - macierz wartosci bledow dla wszystkich par (a,b)
    """
    size_a = len(a_values)
    size_b = len(b_values)
    errors = np.zeros(shape=(size_a, size_b))
    best_a = 0
    best_b = 0
    error_best = np.inf
    for a in range(size_a):
        for b in range(size_b):
            p_y = estimate_a_priori_nb(ytrain)
            p_x_1_y = estimate_p_x_y_nb(Xtrain, ytrain, a_values[a], b_values[b])
            p_y_x = p_y_x_nb(p_y, p_x_1_y, Xval)
            error = classification_error(p_y_x, yval)
            errors[a][b] = error
            if error < error_best:
                error_best = error
                best_a = a_values[a]
                best_b = b_values[b]
    return(error_best, best_a, best_b, errors)
