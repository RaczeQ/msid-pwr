# Metoda z content.py
def p_y_x_nb(p_y, p_x_1_y, X):
    """
    :param p_y: wektor prawdopodobienstw a priori o wymiarach 1xM
    :param p_x_1_y: rozklad prawdopodobienstw p(x=1|y) - macierz MxD
    :param X: dane dla ktorych beda wyznaczone prawdopodobienstwa, macierz NxD
    :return: funkcja wyznacza rozklad prawdopodobienstwa p(y|x) dla kazdej z klas z wykorzystaniem klasyfikatora Naiwnego
    Bayesa. Funkcja zwraca macierz p_y_x o wymiarach NxM.
    """
    result = []
    for n in range(X.shape[0]):
        temp_row = []
        p_x_y = []
        # Oblicz prawdopodobieństwo dla aktualnego rzędu dla każdego k
        for k in [1, 2, 3, 4]:
            temp_p_x_y = 1
            for d in range(X.shape[1]):
                temp_p_x_y *= p_x_1_y[k - 1][d] if X.toarray()[n][d] else 1 - p_x_1_y[k - 1][d]
            p_x_y.append(temp_p_x_y)
        # Oblicz mianownik (taki sam dla każdego k)
        denominator = 0
        for k in [1, 2, 3, 4]:
            denominator += p_x_y[k - 1] * p_y[k - 1]
        # Dla każdego k (wymiar m) podstaw do wzoru
        for m in [1, 2, 3, 4]:
            numerator = p_x_y[m - 1] * p_y[m - 1]
            temp_row.append(numerator / denominator)
        result.append(temp_row)
    return np.asarray(result)

# Dodana pętla w Test.py
class TestPYXNB(TestCase):
    def test_p_y_x_nb(self):
        data = test_data['p_y_x_NB']
        out = p_y_x_nb(data['p_y'], data['p_x_1_y'], data['X'])
        for i in range(out.shape[0]):
            for j in range(out.shape[1]):
                print('{:20.19f}'.format(out[i][j]), '{:20.19f}'.format(data['p_y_x'][i][j]), out[i][j] - data['p_y_x'][i][j])
        self.assertTrue((out == data['p_y_x']).all())