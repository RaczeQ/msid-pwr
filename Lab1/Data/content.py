# --------------------------------------------------------------------------
# -----------------------  Rozpoznawanie Obrazow  --------------------------
# --------------------------------------------------------------------------
#  Zadanie 1: Regresja liniowa
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

import numpy as np
from utils import polynomial


def mean_squared_error(x, y, w):
    '''
    :param x: ciag wejsciowy Nx1
    :param y: ciag wyjsciowy Nx1
    :param w: parametry modelu (M+1)x1
    :return: blad sredniokwadratowy pomiedzy wyjsciami y
    oraz wyjsciami uzyskanymi z wielowamiu o parametrach w dla wejsc x
    '''
    return np.sum((y - polynomial(x, w)) ** 2) / y.shape[0]

def design_matrix(x_train, M):
    '''
    :param x_train: ciag treningowy Nx1
    :param M: stopien wielomianu 0,1,2,...
    :return: funkcja wylicza Design Matrix Nx(M+1) dla wielomianu rzedu M
    '''
    size = x_train.shape[0]
    matrix = np.zeros(shape=(size, M + 1))
    M_vector = np.arange(M+1)
    for i in range(size):
        matrix[i] = x_train[i] ** M_vector
    return matrix

def least_squares(x_train, y_train, M):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param M: rzad wielomianu
    :return: funkcja zwraca krotke (w,err), gdzie w sa parametrami dopasowanego wielomianu, a err blad sredniokwadratowy
    dopasowania
    '''
    matrix_x = design_matrix(x_train, M)
    matrix_x_t = matrix_x.transpose()
    w_values = (np.linalg.inv(matrix_x_t @ matrix_x) @ matrix_x_t) @ y_train
    return w_values, mean_squared_error(x_train, y_train, w_values)

def regularized_least_squares(x_train, y_train, M, regularization_lambda):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param M: rzad wielomianu
    :param regularization_lambda: parametr regularyzacji
    :return: funkcja zwraca krotke (w,err), gdzie w sa parametrami dopasowanego wielomianu zgodnie z kryterium z regularyzacja l2,
    a err blad sredniokwadratowy dopasowania
    '''
    matrix_x = design_matrix(x_train, M)
    matrix_x_t = matrix_x.transpose()
    lambda_matrix = np.eye(M + 1) * regularization_lambda
    w_values = (np.linalg.inv(matrix_x_t @ matrix_x + lambda_matrix) @ matrix_x_t) @ y_train
    return w_values, mean_squared_error(x_train, y_train, w_values)

def model_selection(x_train, y_train, x_val, y_val, M_values):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param x_val: ciag walidacyjny wejscia Nx1
    :param y_val: ciag walidacyjny wyjscia Nx1
    :param M_values: tablica stopni wielomianu, ktore maja byc sprawdzone
    :return: funkcja zwraca krotke (w,train_err,val_err), gdzie w sa parametrami modelu, ktory najlepiej generalizuje dane,
    tj. daje najmniejszy blad na ciagu walidacyjnym, train_err i val_err to bledy na sredniokwadratowe na ciagach treningowym
    i walidacyjnym
    '''
    ls_results = {}
    for m in M_values:
        w, _ = least_squares(x_train, y_train, m)
        err = mean_squared_error(x_val, y_val, w)
        ls_results[err] = w
    best_w = ls_results[min(ls_results.keys())]
    return best_w, mean_squared_error(x_train, y_train, best_w), min(ls_results.keys())

def regularized_model_selection(x_train, y_train, x_val, y_val, M, lambda_values):
    '''
    :param x_train: ciag treningowy wejscia Nx1
    :param y_train: ciag treningowy wyjscia Nx1
    :param x_val: ciag walidacyjny wejscia Nx1
    :param y_val: ciag walidacyjny wyjscia Nx1
    :param M: stopien wielomianu
    :param lambda_values: lista ze wartosciami roznych parametrow regularyzacji
    :return: funkcja zwraca krotke (w,train_err,val_err,regularization_lambda), gdzie w sa parametrami modelu, ktory najlepiej generalizuje dane,
    tj. daje najmniejszy blad na ciagu walidacyjnym. Wielomian dopasowany jest wg kryterium z regularyzacja. train_err i val_err to
    bledy na sredniokwadratowe na ciagach treningowym i walidacyjnym. regularization_lambda to najlepsza wartosc parametru regularyzacji
    '''
    ls_results = {}
    for l in lambda_values:
        w, _ = regularized_least_squares(x_train, y_train, M, l)
        err = mean_squared_error(x_val, y_val, w)
        ls_results[err] = (w, l)
    best_w, best_l = ls_results[min(ls_results.keys())]
    return best_w, mean_squared_error(x_train, y_train, best_w), min(ls_results.keys()), best_l
