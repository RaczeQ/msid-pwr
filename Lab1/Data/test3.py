import pickle
import numpy as np
from content import mean_squared_error, design_matrix, least_squares, regularized_least_squares, model_selection


TEST_DATA = pickle.load(open('test.pkl', mode='rb'))

x_train = TEST_DATA['ms']['x_train']
y_train = TEST_DATA['ms']['y_train']
x_val = TEST_DATA['ms']['x_val']
y_val = TEST_DATA['ms']['y_val']
M_values = TEST_DATA['ms']['M_values']
w = TEST_DATA['ms']['w']
w_computed, _, _ = model_selection(x_train, y_train, x_val, y_val, M_values)
#max_diff = np.max(np.abs(w - w_computed))
#self.assertAlmostEqual(max_diff, 0, 6)