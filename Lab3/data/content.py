# --------------------------------------------------------------------------
# -----------------------  Rozpoznawanie Obrazow  --------------------------
# --------------------------------------------------------------------------
#  Zadanie 3: Regresja logistyczna
#  autorzy: A. Gonczarek, J. Kaczmar, S. Zareba
#  2017
# --------------------------------------------------------------------------

import numpy as np

def sigmoid(x):
    '''
    :param x: wektor wejsciowych wartosci Nx1
    :return: wektor wyjściowych wartości funkcji sigmoidalnej dla wejścia x, Nx1
    '''
    return 1 / (1 + np.exp(-x))

def logistic_cost_function(w, x_train, y_train):
    '''
    :param w: parametry modelu Mx1
    :param x_train: ciag treningowy - wejscia NxM
    :param y_train: ciag treningowy - wyjscia Nx1
    :return: funkcja zwraca krotke (val, grad), gdzie val oznacza wartosc funkcji logistycznej, a grad jej gradient po w
    '''
    w_t = w.transpose()
    val = 0
    size = x_train.shape[0]
    size_w = w.shape[0]
    grad = np.zeros(shape=size_w)
    for n in range(size):
        temp_sigm = sigmoid(w_t @ x_train[n])
        val += (y_train[n] * np.log(temp_sigm) + (1 - y_train[n]) * np.log(1 - temp_sigm))
        grad += (temp_sigm - y_train[n]) * x_train[n]
    return - np.sum(val) / size, grad.reshape(size_w,1) / size




def gradient_descent(obj_fun, w0, epochs, eta):
    '''
    :param obj_fun: funkcja celu, ktora ma byc optymalizowana. Wywolanie val,grad = obj_fun(w).
    :param w0: punkt startowy Mx1
    :param epochs: liczba epok / iteracji algorytmu
    :param eta: krok uczenia
    :return: funkcja wykonuje optymalizacje metoda gradientu prostego dla funkcji obj_fun. Zwraca krotke (w,func_values),
    gdzie w oznacza znaleziony optymalny punkt w, a func_valus jest wektorem wartosci funkcji [epochs x 1] we wszystkich krokach algorytmu
    '''
    values = np.zeros(shape=(epochs, 1))
    w = w0
    val, grad = obj_fun(w)
    for k in range(epochs):
        delta_w = - grad
        w += eta * delta_w
        val, grad = obj_fun(w)
        values[k] = val
    return w, values


def stochastic_gradient_descent(obj_fun, x_train, y_train, w0, epochs, eta, mini_batch):
    '''
    :param obj_fun: funkcja celu, ktora ma byc optymalizowana. Wywolanie val,grad = obj_fun(w,x,y), gdzie x,y oznaczaja podane
    podzbiory zbioru treningowego (mini-batche)
    :param x_train: dane treningowe wejsciowe NxM
    :param y_train: dane treningowe wyjsciowe Nx1
    :param w0: punkt startowy Mx1
    :param epochs: liczba epok
    :param eta: krok uczenia
    :param mini_batch: wielkosc mini-batcha
    :return: funkcja wykonuje optymalizacje metoda stochastycznego gradientu prostego dla funkcji obj_fun. Zwraca krotke (w,func_values),
    gdzie w oznacza znaleziony optymalny punkt w, a func_values jest wektorem wartosci funkcji [epochs x 1] we wszystkich krokach algorytmu. Wartosci
    funkcji do func_values sa wyliczane dla calego zbioru treningowego!
    '''
    values = np.zeros(shape=(epochs, 1))
    w = 0 + w0
    m_range = x_train.shape[0] // mini_batch
    for k in range(epochs):
        start = 0
        end = start + mini_batch
        val, grad = obj_fun(w, x_train[start:end], y_train[start:end])
        for m in range(m_range):
            print(k, "/", epochs, " ", m, m_range)
            delta_w = - grad
            w += eta * delta_w
            start += mini_batch
            end += mini_batch
            val, grad = obj_fun(w, x_train[start:end], y_train[start:end])
        values[k] = obj_fun(w, x_train, y_train)[0]
    return w, values


def regularized_logistic_cost_function(w, x_train, y_train, regularization_lambda):
    '''
    :param w: parametry modelu Mx1
    :param x_train: ciag treningowy - wejscia NxM
    :param y_train: ciag treningowy - wyjscia Nx1
    :param regularization_lambda: parametr regularyzacji
    :return: funkcja zwraca krotke (val, grad), gdzie val oznacza wartosc funkcji logistycznej z regularyzacja l2,
    a grad jej gradient po w
    '''
    # sign = sig(wTxn)
    # - ln p(D|w) = SUM(yn ln sign + (1 - yn)ln(1 - sign))
    # L(w) =  - ln p(D|w) / N
    w_t = w.transpose()
    val = 0
    size = x_train.shape[0]
    size_w = w.shape[0]
    grad = np.zeros(shape=size_w)
    for n in range(size):
        temp_sigm = sigmoid(w_t @ x_train[n])
        val += (y_train[n] * np.log(temp_sigm) + (1 - y_train[n]) * np.log(1 - temp_sigm))
        grad += (temp_sigm - y_train[n]) * x_train[n]
    module_w = np.sum(w[1:] ** 2)
    l = regularization_lambda / 2
    w0_derivative = grad[0] / size
    lambda_w_derivative = ( grad.reshape(size_w,1) / size ) + w * regularization_lambda
    lambda_w_derivative[0] = w0_derivative
    return (- np.sum(val) / size) + (l * module_w), lambda_w_derivative


def prediction(x, w, theta):
    '''
    :param x: macierz obserwacji NxM
    :param w: wektor parametrow modelu Mx1
    :param theta: prog klasyfikacji z przedzialu [0,1]
    :return: funkcja wylicza wektor y o wymiarach Nx1. Wektor zawiera wartosci etykiet ze zbioru {0,1} dla obserwacji z x
     bazujac na modelu z parametrami w oraz progu klasyfikacji theta
    '''
    size_n = x.shape[0]
    w_t = w.transpose()
    y = np.zeros(shape=(size_n,1))
    for n in range(size_n):
        y[n] = 1 if sigmoid(w_t @ x[n]) >= theta else 0
    return y

def f_measure(y_true, y_pred):
    '''
    :param y_true: wektor rzeczywistych etykiet Nx1
    :param y_pred: wektor etykiet przewidzianych przed model Nx1
    :return: funkcja wylicza wartosc miary F
    '''
    y_t_1 = y_true == 1
    y_t_0 = y_true == 0
    y_p_1 = y_pred == 1
    y_p_0 = y_pred == 0
    TP = np.count_nonzero(y_t_1 & y_p_1)
    FP = np.count_nonzero(y_t_0 & y_p_1)
    FN = np.count_nonzero(y_t_1 & y_p_0)
    return 2 * TP / ( 2 * TP + FP + FN )


def model_selection(x_train, y_train, x_val, y_val, w0, epochs, eta, mini_batch, lambdas, thetas):
    '''
    :param x_train: ciag treningowy wejsciowy NxM
    :param y_train: ciag treningowy wyjsciowy Nx1
    :param x_val: ciag walidacyjny wejsciowy Nval x M
    :param y_val: ciag walidacyjny wyjsciowy Nval x 1
    :param w0: wektor poczatkowych wartosci parametrow
    :param epochs: liczba epok dla SGD
    :param eta: krok uczenia
    :param mini_batch: wielkosc mini batcha
    :param lambdas: lista wartosci parametru regularyzacji lambda, ktore maja byc sprawdzone
    :param thetas: lista wartosci progow klasyfikacji theta, ktore maja byc sprawdzone
    :return: funckja wykonuje selekcje modelu. Zwraca krotke (regularization_lambda, theta, w, F), gdzie regularization_lambda
    to najlpszy parametr regularyzacji, theta to najlepszy prog klasyfikacji, a w to najlepszy wektor parametrow modelu.
    Dodatkowo funkcja zwraca macierz F, ktora zawiera wartosci miary F dla wszystkich par (lambda, theta). Do uczenia nalezy
    korzystac z algorytmu SGD oraz kryterium uczenia z regularyzacja l2.
    '''
    size_lambda = len(lambdas)
    size_theta = len(thetas)
    f_values = np.zeros(shape=(size_lambda,size_theta))
    best_f = 0
    best_w = 0
    best_lambda = 0
    best_theta = 0
    for l in range(size_lambda):
        w, _ = stochastic_gradient_descent(helper(lambdas[l]), x_train, y_train, w0, epochs, eta, mini_batch)
        for t in range(size_theta):
            print(lambdas[l],"/",size_lambda," ", thetas[t], "/", size_theta)
            f = f_measure(y_val, prediction(x_val, w, thetas[t]))
            f_values[l][t] = f
            if f > best_f:
                best_f = f
                best_w = w
                best_lambda = lambdas[l]
                best_theta = thetas[t]
    return best_lambda, best_theta, best_w, f_values

def helper(l):
    return lambda w,x,y: regularized_logistic_cost_function(w, x, y, l)